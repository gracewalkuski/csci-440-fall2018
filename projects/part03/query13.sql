.read data.sql

/*
For every situation where student A likes student B, but we have no information
about whom B likes (that is, B does not appear as an ID1 in the Likes table), return
A and B's names and grades. 
*/


select ha.name, ha.grade, hb.name, hb.grade
from highschooler ha, highschooler hb, likes l
where ha.id == l.id1 and hb.id == l.id2 and hb.id not in (
	select id1 from likes);

