.read data.sql

/*
select h.name, h.grade, r.c from highschooler h, friend f where 
h.id == f.id1 group by f.id1 having count(f.id2) = (
	select max(r.c) from
	(select count(id2) as c from friend group by id1) as r);
*/

select h.name, h.id, h.grade from highschooler h, friend f
where h.id == f.id1 group by f.id1 having count(f.id2) = (
	select max(friend_max.num) from
	(select count(id2)as num from friend group by id1) as friend_max);
