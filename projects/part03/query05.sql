.read data.sql

-- Find those students for whom all of their friends are in 
-- different grades from themselves. Return the students' names and 
-- grades.

select distinct name, grade 
from Highschooler 
except select distinct A.name, A.grade 
    from Highschooler A, Highschooler B, Friend F
    where A.ID = F.ID1 and B.ID = F.ID2
    and A.grade = B.grade
except select C.name, C.grade
    from Highschooler C
    where C.ID not in (select Friend.ID1 from Friend)
    or C.ID not in (select Friend.ID2 from Friend)
;