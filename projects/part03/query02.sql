.read data.sql

delete from Likes where (((Likes.ID2, Likes.ID1) not in Likes)
  and (((Likes.ID1, Likes.ID2))
    not in (select Friend.ID1, Friend.ID2 from Friend
      where (Friend.ID1 = Likes.ID1 and Friend.ID2 = Likes.ID2)
        or (Friend.ID1 = Likes.ID2 and Friend.ID2 = Likes.ID1)))
  and (((Likes.ID2, Likes.ID1))
    not in (select Friend.ID1, Friend.ID2 from Friend
      where (Friend.ID1 = Likes.ID1 and Friend.ID2 = Likes.ID2)
        or (Friend.ID1 = Likes.ID2 and Friend.ID2 = Likes.ID1))));
