.read data.sql

/* Find the name of all students who are friend with someone named Gabriel */

/*
find in highschoolers where id has reference to id1 in friends, and 
find where id2 of that friend relation has reference to highschooler via id and named Gabriel
*/

select name from highschooler where id in(
	select id1 from friend where id2 in (
		select id from highschooler where name == 'Gabriel'));
