# Team Members
* Grace Walkuski
* Kendall DiLorenzo
* Ian Hecker
* Michael Hewitt
 
# Idea 1 - Joins
Outline how each of the joins work conceptually as well as in sql - inner join, join using, 
natural join, left outer join, cross join, left join, right join, full join, and self join.
This may end up being too extensive and we can only do the most commonly used joins. This 
will be valueable because we have only really used inner-joins in class and want us and 
future students to be able to use other types of joins. They are very valuable and powerful 
commands that will expand our and future students' abilites.
 
# Idea 2 - Keys
Outline the difference between Primary Keys, Candidate Keys, Foreign Keys, Composite Keys, 
and System Generated Keys. How do you assign them? When do you need them? What are they 
used for? This is a topic that we want to learn more about in order to create stronger 
Relational Schemas and therefore better designed and biult databases. We think that future
students will have similar questions and want to provide a convienient resourse where 
students can find the answer.
 
# Idea 3 - Relationship Suggestions
Outline the different types of relationships and how you choose what relationship to use.
Discuss the difference between full and partial participation. Be able to decide when to use 
different functional dependencies. Define the different types of normal forms and the reasons
to use them. We may only include the ones covered in class, if there are too many, or they
become redundant and tiresome. It would be useful to know how to build better relationships,
so that future databases built by students will be easily navigated. If all of the information
was in one place it would make referencing it easier.

