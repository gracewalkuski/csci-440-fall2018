.read data.sql

-- For each student A who likes a student B where the two are
-- not friends, find if they have a friend C in common (who can
-- introduce them!). For all such trios, return the name and grade of
-- A, B, and C.

select Highschooler.name, Highschooler.grade, H1.name, H1.grade, H2.name, H2.grade
  from (((((Highschooler inner join Likes
    on Likes.ID1 like Highschooler.ID)
  inner join Highschooler as H1
    on H1.ID = Likes.ID2)
  inner join Highschooler as H2)
  inner join Friend as F)
  inner join Friend
    on (Friend.ID1 = Highschooler.ID
      and Friend.ID2 = H2.ID
      and F.ID1 = H1.ID
      and F.ID2 = H2.ID)
      or (Friend.ID1 = Highschooler.ID
        and Friend.ID2 = H2.ID
        and F.ID2 = H1.ID
        and F.ID1 = H2.ID)
      or (Friend.ID2 = Highschooler.ID
        and Friend.ID1 = H2.ID
        and F.ID2 = H1.ID
        and F.ID1 = H2.ID)
      or (Friend.ID2 = Highschooler.ID
        and Friend.ID1 = H2.ID
        and F.ID1 = H1.ID
        and F.ID2 = H2.ID))
where
    (Highschooler.ID, H1.ID) not in (select ID1, ID2 from Friend)
    and (Highschooler.ID, H1.ID) not in (select ID2, ID1 from Friend);
