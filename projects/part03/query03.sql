.read data.sql

insert into Friend
  select Friend.ID1, F.ID2
    from Friend inner join Friend as F
      where Friend.ID2 = F.ID1 and (Friend.ID1, F.ID2) not in Friend;
  -- Union select Friend.ID2, F.ID2
  --   from Friend inner join Friend as F
  --     where Friend.ID1 = F.ID1 and (Friend.ID2, F.ID2) not in Friend and Friend.ID2 not like F.ID2;
