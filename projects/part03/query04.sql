.read data.sql 

-- For every situation where student A likes student B, but 
-- student B likes a different student C, return the names and grades 
-- of A, B, and C.

select A.name, A.grade, B.name, B.grade, C.name, C.grade 
from Highschooler A, Highschooler B, Highschooler C, Likes L1, Likes L2
where A.ID = L1.ID1 and B.ID = L1.ID2
and B.ID = L2.ID1 and C.ID = L2.ID2
and A.ID <> C.ID;

