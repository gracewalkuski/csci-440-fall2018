.read data.sql

-- For every pair of students who both like each other, 
-- return the name and grade of both students. Include each pair 
-- only once, with the two names in alphabetical order.

select H1.name, H1.grade, H2.name, H2.grade
from (Likes L1
inner join Highschooler H1 on H1.ID = L1.ID1
inner join Highschooler H2 on H2.ID = L1.ID2), Likes L2
where H1.ID = L1.ID1 and L1.ID2 = H2.ID
and H1.ID = L2.ID2 and L2.ID1 = H2.ID
and H1.name < H2.name;