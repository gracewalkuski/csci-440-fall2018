.read data.sql

select Highschooler.name, Highschooler.grade, HS.name, HS.grade
  from ((Likes inner join Highschooler
    on Likes.ID1 = Highschooler.ID)
  inner join Highschooler as HS
    on Likes.ID2 = HS.ID)
  where Highschooler.grade - HS.grade > 1;
