.read data.sql

select name, grade from Highschooler
  where Highschooler.ID
    not in (select * from (select Highschooler.ID from ((Highschooler inner join Friend
      on Friend.ID1 like Highschooler.ID)
      inner join Highschooler as HS
        on Friend.ID2 like HS.ID)
          where Highschooler.grade not like HS.grade
      union select Highschooler.ID from ((Highschooler inner join Friend
        on Friend.ID2 like Highschooler.ID)
        inner join Highschooler as HS
          on Friend.ID1 like HS.ID)
            where Highschooler.grade not like HS.grade))
order by grade, name;
